﻿namespace MehanotronikaApp.Tests
{
	public class DataHandlerCarTest
	{
		static DateTime timeNow = DateTime.Now;

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(3)]
		[InlineData(4)]
		[InlineData(5)]
		[InlineData(6)]
		public void TestDataHandlerCar(int value)
		{
			//Arrange
			DataGetter getter = new()
;			DataHandlerCar dataHandlerCar = new(getter);

			//Act
			ShowResultModel? data = dataHandlerCar.HandleData(value, timeNow);

			//Assert
			Assert.IsType<string>(data.CarModel);
		}
	}
}
