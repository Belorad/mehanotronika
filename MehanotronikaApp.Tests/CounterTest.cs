namespace MehanotronikaApp.Tests
{
	public class CounterTest
	{
		[Fact]
		public void TestCounter()
		{
			//Arrange
			Counter counter = new();
			int beat = 6;
			int mark = 1;

			//Act
			counter.Count(ref beat);

			//Assert
			Assert.Equal(mark, beat);
		}
	}
}