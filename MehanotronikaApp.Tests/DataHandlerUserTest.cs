﻿namespace MehanotronikaApp.Tests
{
	public class DataHandlerUserTest
	{
		static DateTime timeNow = DateTime.Now;

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(3)]
		[InlineData(4)]
		[InlineData(5)]
		[InlineData(6)]
		public void TestDataHandlerUser(int value)
		{
			//Arrange
			DataGetter getter = new()
;			DataHandlerUser dataHandlerUser = new(getter);

			//Act
			ShowResultModel? data = dataHandlerUser.HandleData(value, timeNow);

			//Assert
			Assert.IsType<string>(data.Name);
		}
	}
}
