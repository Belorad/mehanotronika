﻿using MehanotronikaApp.Logger;
using System.Collections.ObjectModel;

namespace MehanotronikaApp.Tests
{
	public class DisplayTest
	{
		[Fact]
		public void TestDisplay()
		{
			//Arrange
			ObservableCollection<ShowResultModel> testResult = new()
			{
				new ShowResultModel
				{
					Name = "Вася",
					CarModel = "Лада седан баклажан"
				}
			};

			ShowResultModel Driver = new()
			{
				Name = "Вася"
			};

			ShowResultModel Car = new()
			{
				CarModel = "Лада седан баклажан"
			};

			MyLogger myLogger = new();
			ObservableCollection<ShowResultModel> result = new();

			Display display = new(myLogger);

			//Act
			display.DisplayAllData(Driver, Car, result);

			//Assert
			Assert.Equal(testResult[0].Name, result[0].Name);
			Assert.Equal(testResult[0].CarModel, result[0].CarModel);
		}
	}
}
