﻿namespace MehanotronikaApp.Tests
{
	public class DataGetterTest
	{
		public string[] names = new string[]
		{
			"Вася"
		};

		[Fact]
		public void TestDateGetter()
		{
			//Arrange
			Sample testName = new()
			{
				Name = "Вася"
			};

			DataGetter getter = new();

			//Act
			Sample name = getter.GetData(names);

			//Assert
			Assert.Equal(testName.Name, name.Name);
		}
	}
}
