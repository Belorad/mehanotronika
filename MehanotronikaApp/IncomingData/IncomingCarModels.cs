﻿namespace MehanotronikaApp.IncomingData
{
	public class IncomingCarModels
	{
		public string[] CarModels = new string[]
		{
			"Lada Vesta",
			"Lada Largus",
			"Lada Granta",
			"Lada XRAY",
			"Lada Niva",
			"Renault Logan",
			"Renault Sandero",
			"Renault Duster",
			"Renault Arkana",
			"Ranault Kaptur",
			"Skoda Rapid",
			"Skoda Octavia",
			"Volkswagen Polo",
			"Volkswagen Jetta",
			"Volkswagen Golf",
			"KIA Rio",
			"KIA Cerato",
			"KIA Ceed",
			"Hyundai Solaris",
			"Hyundai i30"
		};
	}
}
