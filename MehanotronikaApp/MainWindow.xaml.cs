﻿using MehanotronikaApp.Logger;
using MehanotronikaApp.Services.Counter;
using MehanotronikaApp.Services.DataHandlerCar;
using MehanotronikaApp.Services.DataHandlerUser;
using MehanotronikaApp.Services.DataReader;
using MehanotronikaApp.Services.DataSetter;
using MehanotronikaApp.Services.Display;
using MehanotronikaApp.ViewModel;
using System;
using System.ComponentModel;
using System.Windows;

namespace MehanotronikaApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly IReadable dataReader;
		private readonly ILoggable myLogger;

		public MainWindow(IReadable dataReader, IDisplayable display, ICountable counter, IHandleableUser handlerUser, IHandleableCar handlerCar, ISettable dataSetter, ILoggable myLogger)
		{
			InitializeComponent();

			this.dataReader = dataReader;
			this.myLogger = myLogger;

			DataContext = new MainWindowViewModel(display, counter, handlerUser, handlerCar, dataSetter, myLogger);

			DisplayList.Items.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
		}

		private void Button_Click_TaskDb(object sender, RoutedEventArgs e)
		{
			myLogger.LogInfo($"DbWindow открыто в {DateTime.Now:G}");
			
			DbWindow dbWindow = new(dataReader)
			{
				Owner = this
			};

			dbWindow.Show();
		}
	}
}
