﻿using MehanotronikaApp.Entities;
using MehanotronikaApp.Logger;
using MehanotronikaApp.Services.Counter;
using MehanotronikaApp.Services.DataGetter;
using MehanotronikaApp.Services.DataHandlerCar;
using MehanotronikaApp.Services.DataHandlerUser;
using MehanotronikaApp.Services.DataReader;
using MehanotronikaApp.Services.DataSetter;
using MehanotronikaApp.Services.Display;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace MehanotronikaApp
{
	internal class Program
	{
		[STAThread]
		public static void Main()
		{
			var host = Host.CreateDefaultBuilder()
				.ConfigureServices(
				services =>
				{
					services.AddSingleton<App>();
					services.AddSingleton<MainWindow>();
					services.AddDbContext<ApplicationContext>(
						options => options.UseNpgsql("Host=localhost;Port=5432;Database=appbd;Username=postgres;Password=postgres"));
					services.AddScoped<IGettable, DataGetter>();
					services.AddScoped<ISettable, DataSetter>();
					services.AddScoped<IHandleableUser, DataHandlerUser>();
					services.AddScoped<IHandleableCar, DataHandlerCar>();
					services.AddScoped<IDisplayable, Display>();
					services.AddScoped<ICountable, Counter>();
					services.AddScoped<IReadable, DataReader>();
					services.AddSingleton<ILoggable, MyLogger>();
				})
				.Build();

			var app = host.Services.GetService<App>();

			app?.Run();
		}
	}
}
