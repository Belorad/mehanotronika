﻿using MehanotronikaApp.Logger;
using MehanotronikaApp.Model;
using MehanotronikaApp.Services.Counter;
using MehanotronikaApp.Services.DataHandlerCar;
using MehanotronikaApp.Services.DataHandlerUser;
using MehanotronikaApp.Services.DataSetter;
using MehanotronikaApp.Services.Display;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using System.Windows.Threading;

namespace MehanotronikaApp.ViewModel
{
	public class MainWindowViewModel : ViewModelBase
	{
		private readonly IDisplayable display;
		private readonly ICountable counter;
		private readonly IHandleableUser handlerUser;
		private readonly IHandleableCar handlerCar;
		private readonly ISettable dataSetter;
		private readonly ILoggable myLogger;

		internal static DispatcherTimer? timer;
		internal static int beat = 6;
		DateTime timeNow;

		bool tickerUserMark;
		bool tickerCarMark;
		AutoResetEvent waitHandlerUser = new(true);
		AutoResetEvent waitHandlerCar = new(true);

		bool isWorkingUserTask;
		bool isWorkingCarTask;

		public ICommand? StartUserTask { get; }
		public ICommand? StopUserTask { get; }
		public ICommand? StartCarTask { get; }
		public ICommand? StopCarTask { get; }

		private CancellationTokenSource? userTokenSource;
		private Task userTask;
		private CancellationTokenSource? carTokenSource;
		private Task carTask;

		private ShowResultModel? addingUser;
		private ShowResultModel? addingCar;
		public ObservableCollection<ShowResultModel> resultCollection = new();

		public ObservableCollection<ShowResultModel> ResultCollection
		{
			get { return resultCollection; }
			set
			{
				resultCollection = value;
				OnPropertyChanged("Adding to Display");
			}
		}

		public ShowResultModel? AddingUser
		{
			get { return addingUser; }
			set
			{
				addingUser = value;
				OnPropertyChanged("Adding User");
			}
		}

		public ShowResultModel? AddingCar
		{
			get { return addingCar; }
			set
			{
				addingCar = value;
				OnPropertyChanged("Adding Car");
			}
		}

		public bool IsWorkingUserTask
		{
			get { return isWorkingUserTask; }
			set
			{
				isWorkingUserTask = value;
				OnPropertyChanged("Working UserTask");
			}
		}

		public bool IsWorkingCarTask
		{
			get { return isWorkingCarTask; }
			set
			{
				isWorkingCarTask = value;
				OnPropertyChanged("Working CarTask");
			}
		}

		public MainWindowViewModel(IDisplayable display, ICountable counter, IHandleableUser handlerUser, IHandleableCar handlerCar, ISettable dataSetter, ILoggable myLogger)
		{
			this.display = display;
			this.counter = counter;
			this.handlerUser = handlerUser;
			this.handlerCar = handlerCar;
			this.dataSetter = dataSetter;
			this.myLogger = myLogger;

			var logTimer = new System.Timers.Timer(900000);
			logTimer.Elapsed += OnTimedEvent;
			logTimer.AutoReset = true;
			logTimer.Enabled = true;
			logTimer.Start();

			timer = new DispatcherTimer()
			{
				Interval = new TimeSpan(0, 0, 1)
			};
			timer.Tick += Ticker;
			timer.Start();
			myLogger.LogInfo($"timer запущен в {DateTime.Now:G}");

			userTokenSource = new CancellationTokenSource();
			userTask = new Task(() => WorkUser(waitHandlerUser, userTokenSource.Token), userTokenSource.Token);
			userTask.Start();
			myLogger.LogInfo($"userTask запущен в {DateTime.Now:G}");

			carTokenSource = new CancellationTokenSource();
			carTask = new Task(() => WorkCar(waitHandlerCar, carTokenSource.Token), carTokenSource.Token);
			carTask.Start();
			myLogger.LogInfo($"carTask запущен в {DateTime.Now:G}");

			StartUserTask = new RelayCommand(c =>
			{
				userTokenSource = new CancellationTokenSource();
				userTask = new Task(() => WorkUser(waitHandlerUser, userTokenSource.Token), userTokenSource.Token);
				userTask.Start();
				myLogger.LogInfo($"userTask вручную запущен в {DateTime.Now:G}");
			}, c => !IsWorkingUserTask);

			StopUserTask = new RelayCommand(c =>
			{
				userTokenSource.Cancel();
				userTokenSource = null;
				userTask = null;
				IsWorkingUserTask = false;
				AddingUser = null;
				myLogger.LogInfo($"userTask вручную остановлен в {DateTime.Now:G}");
			}, c => IsWorkingUserTask);

			StartCarTask = new RelayCommand(c =>
			{
				carTokenSource = new CancellationTokenSource();
				carTask = new Task(() => WorkCar(waitHandlerCar, carTokenSource.Token), carTokenSource.Token);
				carTask.Start();
				myLogger.LogInfo($"carTask вручную запущен в {DateTime.Now:G}");
			}, c => !IsWorkingCarTask);

			StopCarTask = new RelayCommand(c =>
			{
				carTokenSource.Cancel();
				carTokenSource = null;
				carTask = null;
				IsWorkingCarTask = false;
				AddingCar = null;
				myLogger.LogInfo($"carTask вручную остановлен в {DateTime.Now:G}");
			}, c => IsWorkingCarTask);
		}

		private void Ticker(object? sender, EventArgs e)
		{
			counter.Count(ref beat);
			timeNow = DateTime.Now;

			waitHandlerUser.WaitOne();
			waitHandlerCar.WaitOne();
			tickerUserMark = true;

			tickerCarMark = true;

			display.DisplayAllData(AddingUser, AddingCar, ResultCollection);
			waitHandlerUser.Set();
			waitHandlerCar.Set();

			myLogger.LogInfo($"Ticker выполнен в {DateTime.Now:G}");
		}

		private void OnTimedEvent(Object? source, ElapsedEventArgs e)
		{
			myLogger.CreateLogFile();
		}

		private void WorkUser(AutoResetEvent waitHandlerUser, object state)
		{
			IsWorkingUserTask = true;
			var token = (CancellationToken)state;

			while (!token.IsCancellationRequested)
			{
				if (tickerUserMark)
				{
					waitHandlerUser.WaitOne();
					tickerUserMark = false;

					AddingUser = handlerUser.HandleData(beat, timeNow);

					dataSetter.SetData(AddingUser);
					waitHandlerUser.Set();

					myLogger.LogInfo($"WorkUser выполнен в {DateTime.Now:G}");
				}
			}
		}

		private void WorkCar(AutoResetEvent waitHandlerCar, object state)
		{
			IsWorkingCarTask = true;
			var token = (CancellationToken)state;

			while (!token.IsCancellationRequested)
			{
				if (tickerCarMark)
				{
					waitHandlerCar.WaitOne();
					tickerCarMark = false;

					AddingCar = handlerCar.HandleData(beat, timeNow);

					dataSetter.SetData(AddingCar);
					waitHandlerCar.Set();

					myLogger.LogInfo($"WorkCar выполнен в {DateTime.Now:G}");
				}
			}
		}
	}
}

