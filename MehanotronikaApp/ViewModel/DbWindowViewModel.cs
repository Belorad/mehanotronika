﻿using MehanotronikaApp.Model;
using MehanotronikaApp.Services.DataReader;
using System;
using System.Collections.ObjectModel;
using System.Timers;
using System.Windows.Data;

namespace MehanotronikaApp.ViewModel
{
	public class DbWindowViewModel : ViewModelBase
	{
		private readonly IReadable dataReader;
		internal static Timer? dbTimer;

		public ObservableCollection<ShowResultModel> summaryDbCollection = new();

		public static object ItemsLock { get; } = new();

		public ObservableCollection<ShowResultModel> SummaryDbCollection
		{
			get { return summaryDbCollection; }
			set
			{
				summaryDbCollection = value;
				OnPropertyChanged("Showing Db Data");
			}
		}

		public DbWindowViewModel(IReadable dataReader)
		{
			this.dataReader = dataReader;
			BindingOperations.EnableCollectionSynchronization(SummaryDbCollection, ItemsLock);

			dbTimer = new(2000);
			dbTimer.Elapsed += OnTimedEvent;
			dbTimer.AutoReset = true;
			dbTimer.Enabled = true;
		}

		private void OnTimedEvent(Object? source, ElapsedEventArgs e)
		{
			dataReader.ReadData(SummaryDbCollection);
		}
	}
}
