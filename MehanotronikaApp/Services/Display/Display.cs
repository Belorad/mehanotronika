﻿using MehanotronikaApp.Logger;
using MehanotronikaApp.Model;
using System;
using System.Collections.ObjectModel;

namespace MehanotronikaApp.Services.Display
{
	public class Display : IDisplayable
	{
		private readonly ILoggable myLogger;

		public Display(ILoggable myLogger)
		{
			this.myLogger = myLogger;
		}

		public void DisplayAllData(ShowResultModel? user, ShowResultModel? car, ObservableCollection<ShowResultModel> resultCollection)
		{
			if (user != null && car != null)
			{
				ShowResultModel subResult = new()
				{
					Date = user.Date,
					Name = user.Name,
					CarModel = car.CarModel
				};

				resultCollection.Add(subResult);

				myLogger.LogInfo($"DisplayAllData выполнен в {DateTime.Now:G}");
			}
			//else if (user != null && car == null)
			//{
			//	ShowResultModel subResult = new()
			//	{
			//		Date = user.Date,
			//		Name = user.Name,
			//		CarModel = null
			//	};

			//	resultCollection.Add(subResult);
			//}
			//else if (user == null && car != null)
			//{
			//	ShowResultModel subResult = new()
			//	{
			//		Date = car.Date,
			//		Name = null,
			//		CarModel = car.CarModel
			//	};

			//	resultCollection.Add(subResult);
			//}
		}
	}
}
