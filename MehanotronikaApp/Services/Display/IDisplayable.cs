﻿using MehanotronikaApp.Model;
using System.Collections.ObjectModel;

namespace MehanotronikaApp.Services.Display
{
	public interface IDisplayable
	{
		void DisplayAllData(ShowResultModel user, ShowResultModel car, ObservableCollection<ShowResultModel> resultCollection);
	}
}
