﻿using MehanotronikaApp.Entities;
using MehanotronikaApp.Model;
using MehanotronikaApp.ViewModel;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MehanotronikaApp.Services.DataReader
{
	public class DataReader : IReadable
	{
		private readonly ApplicationContext? dbContext;

		public DataReader(ApplicationContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public void ReadData(ObservableCollection<ShowResultModel> summaryDataDb)
		{
			//using var context = new ApplicationContext();

			Users[] usersArray = dbContext.Users
				.AsNoTracking()
				.ToArray();

			Cars[] carsArray = dbContext.Cars
				.AsNoTracking()
				.ToArray();
			
			List<ShowResultModel> uSum = new();

			foreach (var u in usersArray)
			{
				ShowResultModel sum = new()
				{
					Date = u.UserDate,
					Name = u.UserName,
					CarModel = null
				};

				uSum.Add(sum);
			}

			List<ShowResultModel> cSum = new();

			foreach (var s in carsArray)
			{
				ShowResultModel sum = new()
				{
					Date = s.CarDate,
					Name = null,
					CarModel = s.CarModel
				};

				cSum.Add(sum);
			}

			var summaryExclusion = uSum
				.Union(cSum)
				.DistinctBy(d => d.Date);

			var subSummaryInclusion = usersArray
				.Join(carsArray,
				u => u.UserDate,
				c => c.CarDate,
				(u, c) => new
				{
					Date = u.UserDate,
					Name = u.UserName,
					CarModel = c.CarModel
				});

			List<ShowResultModel> summaryInclusion = new();

			foreach (var s in subSummaryInclusion)
			{
				ShowResultModel sum = new()
				{
					Date = s.Date,
					Name = s.Name,
					CarModel = s.CarModel
				};

				summaryInclusion.Add(sum);
			}

			var summaryFinal = summaryInclusion
				.Union(summaryExclusion)
				.DistinctBy(d => d.Date)
				.OrderBy(d => d.Date)
				.Skip(summaryDataDb.Count);

			foreach (var item in summaryFinal)
			{
				ShowResultModel sum = new()
				{
					Date = item.Date,
					Name = item.Name,
					CarModel = item.CarModel
				};

				lock (DbWindowViewModel.ItemsLock)
				{
					summaryDataDb.Add(sum);
				}
			}
		}
	}
}
