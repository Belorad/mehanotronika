﻿using MehanotronikaApp.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MehanotronikaApp.Services.DataReader
{
	public interface IReadable
	{
		void ReadData(ObservableCollection<ShowResultModel> summaryDataDb);
	}
}
