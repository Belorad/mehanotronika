﻿using MehanotronikaApp.Entities;
using MehanotronikaApp.Model;
using System;

namespace MehanotronikaApp.Services.DataSetter
{
	public class DataSetter : ISettable
	{
		public void SetData(ShowResultModel data)
		{
			if (data != null)
			{
				using var context = new ApplicationContext();

				if (data.Name != null)
				{
					Users newUser = new()
					{
						UserName = data.Name,
						UserDate = DateTime.SpecifyKind(data.Date, DateTimeKind.Utc)
					};

					context.Users.Add(newUser);
				}

				if (data.CarModel != null)
				{
					Cars newCar = new()
					{
						CarModel = data.CarModel,
						CarDate = DateTime.SpecifyKind(data.Date, DateTimeKind.Utc)
					};

					context.Cars.Add(newCar);
				}

				context.SaveChanges();
			}
		}
	}
}
