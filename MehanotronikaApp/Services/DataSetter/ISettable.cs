﻿using MehanotronikaApp.Model;
using System.Threading.Tasks;

namespace MehanotronikaApp.Services.DataSetter
{
	public interface ISettable
	{
		void SetData(ShowResultModel data);
	}
}
