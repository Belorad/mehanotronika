﻿using MehanotronikaApp.Model;
using System;

namespace MehanotronikaApp.Services.DataGetter
{
	public class DataGetter : IGettable
	{
		public Sample GetData(string[] incomingData)
		{
			Random rnd = new();

			Sample sample = new()
			{
				Name = incomingData[rnd.Next(incomingData.Length)],
			};

			return sample;
		}
	}
}
