﻿using MehanotronikaApp.Model;

namespace MehanotronikaApp.Services.DataGetter
{
	public interface IGettable
	{
		Sample GetData(string[] incomingData);
	}
}
