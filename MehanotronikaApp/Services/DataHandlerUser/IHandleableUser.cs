﻿using MehanotronikaApp.Model;
using System;
using System.Threading.Tasks;

namespace MehanotronikaApp.Services.DataHandlerUser
{
	public interface IHandleableUser
	{
		ShowResultModel? HandleData(int beat, DateTime timeNow);
	}
}
