﻿using MehanotronikaApp.IncomingData;
using MehanotronikaApp.Model;
using MehanotronikaApp.Services.DataGetter;
using System;

namespace MehanotronikaApp.Services.DataHandlerUser
{
	public class DataHandlerUser : IHandleableUser
	{
		private readonly IGettable dataGetter;

		readonly IncomingNames names = new();

		public DataHandlerUser(IGettable dataGetter)
		{
			this.dataGetter = dataGetter;
		}

		public ShowResultModel? HandleData(int beat, DateTime timeNow)
		{
			if (beat == 3)
			{
				var user = dataGetter.GetData(names.Names);

				ShowResultModel data = new()
				{
					Date = timeNow,
					Name = user.Name,
					CarModel = null
				};

				return data;
			}
			else if (beat == 6)
			{
				var user = dataGetter.GetData(names.Names);

				ShowResultModel data = new()
				{
					Date = timeNow,
					Name = user.Name,
					CarModel = null
				};

				return data;
			}
			else
			{
				return null;
			}
		}
	}
}
