﻿namespace MehanotronikaApp.Services.Counter
{
	public interface ICountable
	{
		void Count(ref int counter);
	}
}
