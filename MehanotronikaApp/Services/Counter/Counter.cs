﻿namespace MehanotronikaApp.Services.Counter
{
	public class Counter : ICountable
	{
		public void Count(ref int counter)
		{
			switch (counter)
			{
				case 1:
					counter++;
					break;
				case 2:
					counter++;
					break;
				case 3:
					counter++;
					break;
				case 4:
					counter++;
					break;
				case 5:
					counter++;
					break;
				case 6:
					counter = 1;
					break;
			}
		}
	}
}
