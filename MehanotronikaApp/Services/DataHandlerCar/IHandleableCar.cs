﻿using MehanotronikaApp.Model;
using System;
using System.Threading.Tasks;

namespace MehanotronikaApp.Services.DataHandlerCar
{
	public interface IHandleableCar
	{
		ShowResultModel? HandleData(int beat, DateTime timeNow);
	}
}
