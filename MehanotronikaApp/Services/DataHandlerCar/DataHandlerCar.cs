﻿using MehanotronikaApp.IncomingData;
using MehanotronikaApp.Model;
using MehanotronikaApp.Services.DataGetter;
using System;

namespace MehanotronikaApp.Services.DataHandlerCar
{
	public class DataHandlerCar : IHandleableCar
	{
		private readonly IGettable dataGetter;

		readonly IncomingCarModels cars = new();

		public DataHandlerCar(IGettable dataGetter)
		{
			this.dataGetter = dataGetter;
		}

		public ShowResultModel? HandleData(int beat, DateTime timeNow)
		{
			if (beat == 2 || beat == 4)
			{
				var car = dataGetter.GetData(cars.CarModels);

				ShowResultModel data = new()
				{
					Date = timeNow,
					Name = null,
					CarModel = car.Name
				};

				return data;
			}
			else if (beat == 6)
			{
				var car = dataGetter.GetData(cars.CarModels);

				ShowResultModel data = new()
				{
					Date = timeNow,
					Name = null,
					CarModel = car.Name
				};

				return data;
			}
			else
			{
				return null;
			}
		}
	}
}
