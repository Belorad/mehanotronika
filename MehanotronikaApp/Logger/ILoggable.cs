﻿using System.Collections.Generic;

namespace MehanotronikaApp.Logger
{
	public interface ILoggable
	{
		void CreateLogFile();
		void LogInfo(string text);
	}
}
