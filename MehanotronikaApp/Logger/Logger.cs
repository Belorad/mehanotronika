﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MehanotronikaApp.Logger
{
	public class MyLogger : ILoggable
	{
		static List<string> summaryInfo1 = new();
		static List<string> summaryInfo2 = new();

		bool list1 = true;
		bool list2 = false;

		public void CreateLogFile()
		{
			string fileName = $"logfile-{DateTime.Now:dd.MM.yyyy_HH.mm}.txt";
			string path = @"C:\Users\Yoba\OneDrive\Документы\MehanotronikaApp\" + fileName;

			if (summaryInfo1.Count != 0)
			{
				list1 = false;
				list2 = true;

				using (StreamWriter sw = File.CreateText(path))
				{
					foreach (var line in summaryInfo1)
					{
						sw.WriteLine(line);
					}
				}

				summaryInfo1.Clear();
			}
			else if (summaryInfo2.Count != 0)
			{
				list1 = true;
				list2 = false;

				using (StreamWriter sw = File.CreateText(path))
				{
					foreach (var line in summaryInfo2)
					{
						sw.WriteLine(line);
					}
				}

				summaryInfo2.Clear();
			}
		}

		public void LogInfo(string text)
		{
			if (list1)
			{
				summaryInfo1.Add(text);
			}
			else if (list2)
			{
				summaryInfo2.Add(text);
			}
		}
	}
}
