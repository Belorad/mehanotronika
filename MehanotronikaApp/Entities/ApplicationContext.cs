﻿using Microsoft.EntityFrameworkCore;

namespace MehanotronikaApp.Entities
{
	public class ApplicationContext : DbContext
	{
		public DbSet<Users> Users { get; set; }
		public DbSet<Cars> Cars { get; set; }

		public ApplicationContext()
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=appbd;Username=postgres;Password=postgres");
		}

		public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
		{
			Database.EnsureDeleted();
			Database.EnsureCreated();
		}
	}
}
