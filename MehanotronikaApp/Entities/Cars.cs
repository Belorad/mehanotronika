﻿using System;

namespace MehanotronikaApp.Entities
{
	public class Cars
	{
		public int Id { get; set; }
		public string CarModel { get; set; }
		public DateTime CarDate { get; set; }
	}
}
