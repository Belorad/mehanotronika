﻿using System;

namespace MehanotronikaApp.Entities
{
	public class Users
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public DateTime UserDate { get; set; }
	}
}
