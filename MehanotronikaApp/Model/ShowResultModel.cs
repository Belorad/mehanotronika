﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MehanotronikaApp.Model
{
	public class ShowResultModel : INotifyPropertyChanged
	{
		private DateTime date;
		private string name;
		private string carModel;

		public DateTime Date
		{
			get { return date; }
			set
			{
				date = value;
				OnPropertyChanged("Date");
			}
		}

		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				OnPropertyChanged("Name");
			}
		}

		public string CarModel
		{
			get { return carModel; }
			set
			{
				carModel = value;
				OnPropertyChanged("CarModel");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName] string prop = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(prop));
			}
		}
	}
}
