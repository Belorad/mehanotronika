﻿using System;

namespace MehanotronikaApp.Model
{
	public class Sample
	{
		public DateTime? Date { get; set; }
		public string Name { get; set; }
	}
}
