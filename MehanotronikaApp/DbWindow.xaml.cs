﻿using MehanotronikaApp.Services.DataReader;
using MehanotronikaApp.ViewModel;
using System.ComponentModel;
using System.Windows;

namespace MehanotronikaApp
{
	/// <summary>
	/// Логика взаимодействия для DbWindow.xaml
	/// </summary>
	public partial class DbWindow : Window
	{
		public DbWindow(IReadable dataReader)
		{
			InitializeComponent();

			DataContext = new DbWindowViewModel(dataReader);

			SummaryDataListReader.Items.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));

			Closing += DbWindow_Closing;
		}

		private void DbWindow_Closing(object? sender, CancelEventArgs e)
		{
			DbWindowViewModel.dbTimer.Close();
		}
	}
}
